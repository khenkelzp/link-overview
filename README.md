# link-overview
A website that provides links to all apps and environments (and more).

# How to build
- `npm i` to install packages (required)
- `npm run build` for building once (use in production)
- `npm start` for building and watching files (for development)

# Setup
Everything is read from config files. The only required config file is `/config/main.json`.

## `/config/main.json`
| Field             | Type          | Example                                         | Description                         |
| -----             | ----          | -------                                         | -----------                         |
| companyName       | String        | `"My Company"`                                  | Used for branding                   |
| logo              | String        | `"logo.png"`                                    | Used for branding                   |
| appGroups         | Array<String> | `["projectTeam1.json", "projectTeam2.json"]`    | Paths to JSON files for *App Groups*. If you have multiple teams/projects that use different environments and apps it makes sense to create one configuration file for each. See [App Group](#app-group) on how to define app groups.   |
| uniqueLinkGroups  | Array<String> | `["qaLinks.json", "devLinks.json"]`             | Path to JSON file for *Unique Link Groups*. An example for using multiple configuration files would be if you have a huge collection of useful links and want to split them by main categories (eg. "General", "Development", "Testing"). See [Unique Link Group](#unique-link-group) on how to define unique link groups.  |

## App Group
An app group contains information about the environment and apps. For each app group you have to create a new JSON file and reference it accordingly in `main.json`.

| Field         | Type                                | Example               | Description                         |
| -----         | ----                                | -------               | -----------                         |
| name          | String                              | `"My App Group"`      | Name of the app group.               |
| icon          | String                              | `"fa fa-home"`        | Must be a [font awesome](https://fontawesome.com/icons) class name!  |
| environments  | Array<[Environment](#environment)>  | `[{ ... }, { ... }]`  | Defines the environments. See [Environment](#environment) on how to define environments.   |
| apps          | Array<[App](#app)>                  | `[{ ... }, { ... }]`  | Defines the apps. See [App](#app) on how to define apps.  |

### Environment
| Field     | Type    | Example           | Description                                                 |
| -----     | ----    | -------           | -----------                                                 |
| name      | String  | `"Production"`    | Name of the environment.                                    |
| host      | String  | `"mywebsite.com"` | The environment host name. Must not include http protocol!  |
| useHttps  | Boolean | `true`            | If true, will use `https://`, otherwise `http://`.          |

### App
| Field         | Type                                | Example                   | Description                                 |
| -----         | ----                                | -------                   | -----------                                 |
| name          | String                              | `"My App"`                | Name of the app.                            |
| path          | String                              | `"/path/to/the/app.html"` | The app path. Must not include host name!   |
| environments  | Array<[Environment](#environment)>  | `[{ ... }, { ... }]`      | Optional. Overrides the environment for this specific app/variant. Use this eg. if you have to use a different host name for this app or if the app is only available on certain environments. Note that once `environments` exists the app will only be available for an environment if it's listed (at least with the `name` property)! Environment names must match the ones defined in the app group. Elements take the same properties as [Environment](#environment). |
| variants      | Array<[App](#app)>                  | `[{ ... }, { ... }]`      | Optional. Each app variant will be shown as a secondary button within the same app. Used eg. for shortcuts to specific parts of the app. Technically can be any path though. Independent of the primary app path, must be a FULL path. Elements take the same properties as [App](#app) (excluding `variants`). |

## Unique Link Group
An unique link group contains links that are not environment specific. For each unique link group you have to create a new JSON file and reference it accordingly in `main.json`.

| Field         | Type                          | Example               | Description                         |
| -----         | ----                          | -------               | -----------                         |
| name          | String                        | `"My Useful Links"`   | Name of the unique link group.              |
| icon          | String                        | `"fa fa-home"`        | Must be a [font awesome](https://fontawesome.com/icons) class name!  |
| categories    | Array<[Category](#category)>  | `[{ ... }, { ... }]`  | Defines the categories and links. See [Category](#category) on how to define categories.   |

### Category
The links will be grouped under categories. Each category contains a set of links.

| Field     | Type                  | Example               | Description                       |
| -----     | ----                  | -------               | -----------                       |
| name      | String                | `"Documentation"`     | Name of the category.             |
| icon      | String                | `"fa fa-home"`        | Must be a [font awesome](https://fontawesome.com/icons) class name!   |
| links     | Array<[Link](#link)>  | `[{ ... }, { ... }]`  | Defines the links. See [Link](#link) on how to define links. |

### Link
| Field     | Type                  | Example                     | Description        |
| -----     | ----                  | -------                     | -----------        |
| name      | String                | `"Google"`                  | Name of the link.  |              
| url       | String                | `"https://www.google.com"`  | Full URL of the link.   |