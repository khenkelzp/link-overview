import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import {
  Paper,
  Box,
  Typography,
  Icon,
  ListItemIcon,
  GridList,
  GridListTile,
  List,
  ListItem,
  ListItemText,
  Divider,
  ListItemSecondaryAction,
} from '@material-ui/core';

const UniqueLinks = (props) => {
  const { uniqueLinksGroup, leftOffset } = props;
  const useStyles = makeStyles(() => ({
    root: {
      width: `calc(100% - ${leftOffset}px)`,
      marginLeft: leftOffset,
      padding: '16px 16px 0px 32px'
    },
    categoryIcon: {
      width: '1.2em'
    },
    externalLinkIcon: {
      color: 'rgba(0, 0, 0, 0.54)'
    }
  }));

  const classes = useStyles();
  const renderCategoryList = (category) => {
    return (
      <GridListTile key={category.name} cols={1}>
        <Box mb={2} mr={2}>
          <Paper elevation={3}>
            <List>
              <ListItem>
                {category.icon && (
                  <ListItemIcon>
                    <Icon className={`${category.icon} ${classes.categoryIcon}`} />
                  </ListItemIcon>
                )}
                <ListItemText>{category.name}</ListItemText>
              </ListItem>
              <Divider />
              {category.links.map(link => (
                <ListItem button component="a" href={link.url} target="_blank" key={link.name}>
                  <ListItemText>{link.name}</ListItemText>
                  <ListItemSecondaryAction>
                    <Icon className={`fa fa-external-link-alt ${classes.externalLinkIcon}`} fontSize="small" />
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </Paper>
        </Box>
      </GridListTile>
    );
  };

  return (
    <div key={uniqueLinksGroup.name} className={classes.root}>
      <Box mt={2} mb={2}>
        <Typography display="block" component="header" align="left">
          <Typography variant="h4" component="h1" display="inline">{uniqueLinksGroup.name}</Typography>
        </Typography>
      </Box>
      <GridList cols={3} cellHeight="auto" spacing={0}>
        {uniqueLinksGroup.categories.map(renderCategoryList)}
      </GridList>
    </div>
  );
}

UniqueLinks.propTypes = {
  uniqueLinksGroup: PropTypes.object.isRequired,
  leftOffset: PropTypes.number.isRequired
};

export default UniqueLinks;
