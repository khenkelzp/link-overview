import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import {
  Drawer,
  List,
  ListItem,
  ListItemText,
  Divider,
  ListItemAvatar,
  Avatar,
  ListItemIcon,
  Icon,
  IconButton,
} from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const SideMenu = (props) => {
  const {
    companyName,
    logo,
    appGroups,
    uniqueLinkGroups,
    onClickMenuItem,
    selectedGroup,
    width,
  } = props;

  const theme = useTheme();
  const isLargeScreen = useMediaQuery(theme.breakpoints.up('lg'));

  const useStyles = makeStyles(() => ({
    root: {
      display: 'flex',
    },
    drawer: {
      width,
      flexShrink: 0,
    },
    menuButton: {
      [theme.breakpoints.up('lg')]: {
        display: 'none',
      },
      color: 'rgba(0, 0, 0, 0.54)',
    },
    headerListItem: {
      paddingBottom: 16,
      cursor: 'pointer'
    },
    drawerPaper: {
      width,
    },
    icon: {
      width: '1.2em'
    }
  }));

  const [isMobileOpen, setMobileOpen] = React.useState(false);

  const handleMenuItemClicked = (group) => {
    setMobileOpen(false);
    onClickMenuItem(group);
  };

  const classes = useStyles();
  const renderListItem = (group, index) => {
    return (
      <ListItem button onClick={() => handleMenuItemClicked(group)} key={`${group}${index}`} selected={group === selectedGroup}>
        {group.icon && (
          <ListItemIcon>
            <Icon className={`${classes.icon} ${group.icon}`} />
          </ListItemIcon>)}
        <ListItemText primary={group.name}></ListItemText>
      </ListItem>
    );
  };

  const renderMenuDrawer = (variant, isOpen) => {
    return (
      <Drawer
        variant={variant}
        open={isOpen}
        onClose={variant === 'temporary' ? () => setMobileOpen(false) : () => {}}
        anchor="left"
        className={classes.drawer}
        classes={{ paper: classes.drawerPaper}}
        PaperProps={{ elevation: 2 }}
      >
      <List>
        <ListItem onClick={() => onClickMenuItem(appGroups[0])} className={classes.headerListItem}>
          {logo && (
            <ListItemAvatar>
              <Avatar alt="Logo" src={logo} />
            </ListItemAvatar>)}
          <ListItemText primary={`${companyName ? companyName + ' ' : ''}Link Overview`}></ListItemText>
        </ListItem>
        <Divider />
        {appGroups.map((group, index) => renderListItem(group, index, selectedGroup))}
        <Divider />
        {uniqueLinkGroups.map((group, index) => renderListItem(group, index, selectedGroup))}
      </List>
    </Drawer>
    );
  };

  if (!isLargeScreen) {
    return (
      <div>
        <Drawer variant="permanent" anchor="left" PaperProps={{ elevation: 2 }}>
          <IconButton
            color="inherit"
            onClick={() => setMobileOpen(true)}
            className={classes.menuButton}
          >
            <Icon className="fas fa-bars" />
          </IconButton>
        </Drawer>
        {renderMenuDrawer('temporary', isMobileOpen)}
      </div>
    );
  }

  return renderMenuDrawer('permanent', true);
};

SideMenu.propTypes = {
  companyName: PropTypes.string,
  logo: PropTypes.any,
  appGroups: PropTypes.array.isRequired,
  uniqueLinkGroups: PropTypes.array.isRequired,
  onClickMenuItem: PropTypes.func.isRequired,
  selectedGroup: PropTypes.object.isRequired,
  width: PropTypes.number.isRequired
};

export default SideMenu;