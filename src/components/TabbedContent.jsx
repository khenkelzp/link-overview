import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import { Paper, Tabs, Tab, useMediaQuery } from '@material-ui/core';
import storageService from './../services/storageService';

const TabbedContent = (props) => {
  const theme = useTheme();
  const isLargeScreen = useMediaQuery(theme.breakpoints.up('lg'));

  const { parentName, tabs, leftOffset } = props;
  const useStyles = makeStyles(() => ({
    root: {
      width: `calc(100% - ${leftOffset}px -10px)`,
      marginLeft: leftOffset,
    },
    tab: {
      minWidth: isLargeScreen ? 88 : 70,
    },
  }));
  
  const savedTabs = storageService.get(storageService.TABS) && JSON.parse(storageService.get(storageService.TABS));
  const [selectedTab, setSelectedTab] = React.useState(savedTabs || {});
  const handleTabChange = (event, newTab) => {
    const selectedTabs = {
      ...selectedTab,
      [parentName]: newTab,
    };
    storageService.save(storageService.TABS, JSON.stringify(selectedTabs));
    setSelectedTab(selectedTabs);
  };
  const classes = useStyles();
  return (
    <div key={parentName} className={classes.root}>
      <Paper square>
        <Tabs value={selectedTab[parentName] || 0} onChange={handleTabChange} variant="fullWidth">
          {tabs.map((tab) => 
            <Tab key={`${tab.name}${parentName}`} label={tab.name} className={classes.tab}></Tab>)
          }
        </Tabs>
      </Paper>
      {tabs[selectedTab[parentName] || 0].pageContent}
    </div>
  );
}

TabbedContent.propTypes = {
  parentName: PropTypes.string.isRequired,
  tabs: PropTypes.array.isRequired,
  leftOffset: PropTypes.number.isRequired
};

export default TabbedContent;
