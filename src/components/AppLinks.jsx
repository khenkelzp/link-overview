import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { Paper, Typography, Button, Grid, IconButton, Box } from '@material-ui/core';
import storageService from './../services/storageService';
import starService from './../services/starService';

const useStyles = makeStyles(() => ({
  tile: {
    marginTop: 16
  },
  secondaryIconGrid: {
    textAlign: 'right'
  },
  star: {
    padding: 4,
    margin: '4px 4px 0 0'
  }
}));

const getLink = (app, environment, overrideEnvironment = {}) => {
  const useHttps = typeof overrideEnvironment.useHttps === 'boolean' ? overrideEnvironment.useHttps : environment.useHttps;
  return `${useHttps ? 'https' : 'http'}://${overrideEnvironment.host || environment.host}${app.path}`;
};

const renderButton = (app, environment, options = {}, key) => {
  const overrideEnvironment = app.environments && 
    app.environments.find(appEnvironment => appEnvironment.name.toLowerCase() === environment.name.toLowerCase());
  if (app.environments && !overrideEnvironment) {
    return null;
  }
  return (
    <Grid item key={`${app.name}${environment.name}${key}`}>
      <Button variant="contained" color={options.color} href={getLink(app, environment, overrideEnvironment)} target="_blank">
        {app.name}
      </Button>
    </Grid>
  );
};

const AppLinks = (props) => {
  const { environment, apps } = props;
  const classes = useStyles();

  const starredAppNames = storageService.get(storageService.STARRED_APPS) ? JSON.parse(storageService.get(storageService.STARRED_APPS)) : [];
  const [sortedApps, setSortedApps] = React.useState(starService.sortApps(apps, starredAppNames));

  const handleStarIconClicked = (app) => {
    const isAppStarred = starService.isAppStarred(app, starredAppNames);
    let newStarredAppNames;
    if (isAppStarred) {
      newStarredAppNames = starredAppNames.filter(starredAppName => starredAppName !== app.name);
      storageService.save(storageService.STARRED_APPS, JSON.stringify(newStarredAppNames));
    } else {
      newStarredAppNames = [...starredAppNames, app.name];
      storageService.save(storageService.STARRED_APPS, JSON.stringify(newStarredAppNames));
    }
    setSortedApps(starService.sortApps(apps, newStarredAppNames));
  };

  const renderApp = (app, environment) => {
    const isAppStarred = starService.isAppStarred(app, starredAppNames);
    return (
      <Grid item xs={12} key={`${app.name}${environment.name}`} className={classes.tile}>
        <Paper>
          <Box pl={2} pr={1} pt={1} pb={2}>
            <Grid container>
              <Grid item xs={11}>
                <Typography variant="overline" display="block" align="left" gutterBottom>{app.name}</Typography>
              </Grid>
              <Grid item xs={1} className={classes.secondaryIconGrid}>
                <IconButton className={`${isAppStarred ? 'fas' : 'far'} fa-star ${classes.star}`} onClick={() => handleStarIconClicked(app)} />
              </Grid>
            </Grid>
            <Grid container spacing={2}>
              {app.path && renderButton(app, environment, { color: 'primary' }, 'main')}
              {app.variants && app.variants.map((variant, index) => renderButton(variant, environment, {}, index))}
            </Grid>
          </Box>
        </Paper>
      </Grid>
    );
  };
  
  return (
    <Box px={4} py={2}>
      <Box mt={2}>
        <Typography display="block" component="header" align="left">
          <Typography variant="h4" component="h1" display="inline">{environment.name}</Typography>
          <Box ml={1} display="inline">
            <Typography variant="subtitle1" component="h2" display="inline">&nbsp;({environment.host})</Typography>
          </Box>
        </Typography>
      </Box>
      <Grid container>
        {sortedApps.map(app => renderApp(app, environment))}
      </Grid>
    </Box>
  );
}

AppLinks.propTypes = {
  environment: PropTypes.object.isRequired,
  apps: PropTypes.array.isRequired,
};

export default AppLinks;
