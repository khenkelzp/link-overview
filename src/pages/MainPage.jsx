import React from 'react';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TabbedContent from '../components/TabbedContent.jsx';
import UniqueLinks from '../components/UniqueLinks.jsx';
import AppLinks from '../components/AppLinks.jsx';
import SideMenu from '../components/SideMenu.jsx';
import configService from './../services/configService';
import storageService from './../services/storageService';

const getGroupFromName = (appGroups, uniqueLinkGroups, name) => {
  if (!name) {
    return undefined;
  }
  return appGroups.find(group => group.name === name) ||
    uniqueLinkGroups.find(group => group.name === name);
};

const SIDE_MENU_WIDTH = 290;

const MainPage = () => {
  const theme = useTheme();
  const isLargeScreen = useMediaQuery(theme.breakpoints.up('lg'));
  const contentOffset = isLargeScreen ? SIDE_MENU_WIDTH : 50;

  const companyName = configService.getCompanyName();
  const appGroups = configService.getAppGroups();
  const uniqueLinkGroups = configService.getUniqueLinkGroups();

  const savedGroup = getGroupFromName(appGroups, uniqueLinkGroups, storageService.get(storageService.MENU_ITEM));
  const [selectedGroup, setSelectedGroup] = React.useState(savedGroup || appGroups[0]);

  const generateTabs = (environments, apps) => {
    return environments.map(environment => {
      return {
        ...environment,
        pageContent: <AppLinks environment={environment} apps={apps}></AppLinks>
      };
    });
  };

  const handleMenuItemClicked = (newSelectedGroup) => {
    storageService.save(storageService.MENU_ITEM, newSelectedGroup.name);
    setSelectedGroup(newSelectedGroup);
  };
  return (
    <div>
      <SideMenu
        companyName={companyName}
        logo={configService.getLogo()}
        appGroups={appGroups}
        uniqueLinkGroups={uniqueLinkGroups}
        onClickMenuItem={handleMenuItemClicked}
        selectedGroup={selectedGroup}
        width={SIDE_MENU_WIDTH}
      />
      {appGroups.find(appGroup => appGroup === selectedGroup) && 
        <TabbedContent
          tabs={generateTabs(selectedGroup.getEnvironments(), selectedGroup.getApps())}
          parentName={selectedGroup.getName()}
          leftOffset={contentOffset} />}
      {uniqueLinkGroups.find(uniqueLinksGroup => uniqueLinksGroup === selectedGroup) && 
        <UniqueLinks uniqueLinksGroup={selectedGroup} leftOffset={contentOffset} />}
    </div>
  );
};

export default MainPage;
