const isAppStarred = (app, starredAppNames) => {
  return starredAppNames.includes(app.name);
};

const sortApps = (apps, starredAppNames) => {
  const appsCopy = [...apps];
  return starredAppNames && starredAppNames.length > 0 ? appsCopy.sort((a, b) => {
    const isStarredA = starredAppNames.some(starredAppName => starredAppName === a.name);
    const isStarredB = starredAppNames.some(starredAppName => starredAppName === b.name);
    if (isStarredA && !isStarredB) return -1;
    if (!isStarredA && isStarredB) return 1;
    return 0;
  }) : apps;
};

export default {
  isAppStarred,
  sortApps,
};
