const KEYS = {
  MENU_ITEM: 'selectedMenuItem',
  TABS: 'selectedTabs',
  STARRED_APPS: 'starredApps'
};

const get = (key) => {
  if (!window.localStorage) {
    return;
  }
  return window.localStorage.getItem(key);
};

const save = (key, value) => {
  if (!window.localStorage) {
    return;
  }
  window.localStorage.setItem(key, value);
};

const remove = (key) => {
  if (!window.localStorage) {
    return;
  }
  window.localStorage.removeItem(key);
};

const clear = (key, value) => {
  if (!window.localStorage) {
    return;
  }
  window.localStorage.clear();
};

export default {
  get,
  save,
  remove,
  clear,
  ...KEYS,
};
