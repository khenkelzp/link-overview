const mainConfig = require('./../../config/main.json');

const getConfigProperty = (config, key) => {
  return config[key];
};

const appGroups = (mainConfig.appGroups && mainConfig.appGroups.map((configFileName) => {
  const appGroupConfig =  require(`./../../config/${configFileName}`);
  return {
    ...appGroupConfig,
    getName: () => getConfigProperty(appGroupConfig, 'name'),
    getApps: () => getConfigProperty(appGroupConfig, 'apps'),
    getEnvironments: () => getConfigProperty(appGroupConfig, 'environments'),
  };
})) || [];

const uniqueLinkGroups = (mainConfig.uniqueLinkGroups && mainConfig.uniqueLinkGroups.map((configFileName) => {
  const uniqueLinkGroupConfig = require(`./../../config/${configFileName}`);
  return {
    ...uniqueLinkGroupConfig,
    getName: () => getConfigProperty(uniqueLinkGroupConfig, 'name')
  };
})) || [];

const logo = require(`./../../config/${getConfigProperty(mainConfig, 'logo')}`);

const getCompanyName = () => {
  return getConfigProperty(mainConfig, 'companyName');
};

const getAppGroups = () => {
  return appGroups;
};

const getUniqueLinkGroups = () => {
  return uniqueLinkGroups;
};

const getLogo = () => {
  return logo;
};

export default {
  getCompanyName,
  getAppGroups,
  getUniqueLinkGroups,
  getLogo,
};
